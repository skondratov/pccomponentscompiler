package ccp.complect.pageControllers;

import java.util.List;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import ccp.complect.data.dao.QuestionDAO;
import ccp.complect.data.entity.Answer;
import ccp.complect.data.entity.Question;

public class InterviewCtr extends SelectorComposer<Component>{

	private static final long serialVersionUID = 1L;

	QuestionDAO qD = new QuestionDAO();
	Set<Answer> curAnsws;
	int questIter=0;
	
	@Wire
	Label lbQuest;
	@Wire
	Button btAnsw;
	@Wire
	Vlayout rdAnsw; 
	@Wire
	Radiogroup rgAnsw;
	@Wire
	Window winQue;
	
    @Override
    public void doAfterCompose(Component comp) throws Exception{
        super.doAfterCompose(comp);
        Question qe=qD.getStartQuestion();
        if(qe!=null){
        	generateQuestion(qe);
        }else{
        	Clients.showNotification("���������� �������� ������ ������");
        }
    }
    
    @Listen("onClick=#btAnsw")
    public void genNextStep(){
    	if(rgAnsw.getSelectedItem()!=null){
    		generateQuestion(((Answer)curAnsws.toArray()[rgAnsw.getSelectedItem().getValue()]).getNextQuestion());
    	}
    }
    
    // ��������� ������ ������� � �������
    public void generateQuestion(Question qe){
    	questIter++;
    	winQue.setTitle("������ �"+questIter);
        lbQuest.setValue(qe.getDescript());
        curAnsws=qe.getAnswers();
        
        while(rdAnsw.getChildren().size()>0)
        {
        	rdAnsw.removeChild(rdAnsw.getChildren().get(0));
        }
        
        for(int i=0;i<curAnsws.size();i++){
        	Radio newrd=new Radio();
        	Answer getA=(Answer)curAnsws.toArray()[i];
        	newrd.setValue(i);
        	newrd.setLabel(getA.getDescript());
        	rdAnsw.appendChild(newrd);
        }
        
    }
    
}
