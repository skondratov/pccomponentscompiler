package ccp.complect.data.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import ccp.complect.data.entity.Answer;


public class AnswerDAO extends DAO{
    public Answer createAnswer(String descript)
            throws Exception {
        try {
            begin();
            Answer ans = new Answer(descript);
            getSession().save(ans);
            commit();
            return ans;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("���������� ������� �����: " + descript, e);
        }
    }
    
    public Answer updateAnswer(Answer ans)throws Exception {
        try {
            begin();
            getSession().update(ans);
            commit();
            return ans;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("���������� �������� �����: " + ans.getDescript() + e.getMessage(), e);
        }
    }

    public Answer getAnswer(String descript) throws Exception {
        try {
            begin();
            Query q = getSession().createQuery("from Answer where descript = :descript");
            q.setString("descript", descript);
            Answer user = (Answer) q.uniqueResult();
            commit();
            return user;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("���������� �������� ����� �� ����:  " + descript, e);
        }
    }
    
    public List<Answer> getAllAnswer() throws Exception{
        try {
            begin();
            Query q = getSession().createQuery("from Answer");
            List<Answer> answs = q.list();
            commit();
            return answs;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("���������� �������� ������ �������", e);
        }	
    }
}
