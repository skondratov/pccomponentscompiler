package ccp.complect.data.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import ccp.complect.data.entity.ref.RAMType;

public class RAMTypeDAO extends DAO{
    public List<RAMType> getAllRAMType() throws Exception{
        try {
            begin();
            Query q = getSession().createQuery("from RAMType");
            List<RAMType> rms = q.list();
            commit();
            return rms;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("���������� �������� ������ ����� ������", e);
        }	
    }
}
