package ccp.complect.data.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import ccp.complect.data.entity.ref.Socket;

public class SocketDAO extends DAO{
    public List<Socket> getAllSocket() throws Exception{
        try {
            begin();
            Query q = getSession().createQuery("from Socket");
            List<Socket> sos = q.list();
            commit();
            return sos;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("���������� �������� ������ ����� ������", e);
        }	
    }
}
