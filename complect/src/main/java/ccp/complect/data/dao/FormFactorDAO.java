package ccp.complect.data.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import ccp.complect.data.entity.ref.FormFactor;

public class FormFactorDAO extends DAO{
    public List<FormFactor> getAllFormFactor() throws Exception{
        try {
            begin();
            Query q = getSession().createQuery("from FormFactor");
            List<FormFactor> ffs = q.list();
            commit();
            return ffs;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("���������� �������� ������ ������������", e);
        }	
    }
}
