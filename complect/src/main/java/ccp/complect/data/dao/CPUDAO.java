package ccp.complect.data.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import ccp.complect.data.entity.CPU;


public class CPUDAO extends DAO{
    public CPU createCPU()
            throws Exception {
        try {
            begin();
            CPU cpu = new CPU();
            getSession().save(cpu);
            commit();
            return cpu;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("Невозможно создать cpu" , e);
        }
    }
    
    public CPU createCPU(CPU curCpu)
            throws Exception {
        try {
            begin();
            CPU cpu = curCpu;
            getSession().save(cpu);
            commit();
            return cpu;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("Невозможно создать cpu" , e);
        }
    }
    
    public CPU updateCPU(CPU cpu)throws Exception {
        try {
            begin();
            getSession().update(cpu);
            commit();
            return cpu;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("Невозможно обновить cpu", e);
        }
    }
    
    public List<CPU> getAllCPU() throws Exception{
        try {
            begin();
            Query q = getSession().createQuery("from CPU");
            List<CPU> cpus = q.list();
            commit();
            return cpus;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("Невозможно получить список CPU", e);
        }	
    }
}
