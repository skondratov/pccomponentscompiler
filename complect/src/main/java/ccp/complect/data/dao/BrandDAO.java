package ccp.complect.data.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import ccp.complect.data.entity.ref.Brand;

public class BrandDAO extends DAO{
    public List<Brand> getAllBrand() throws Exception{
        try {
            begin();
            Query q = getSession().createQuery("from Brand");
            List<Brand> brands = q.list();
            commit();
            return brands;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("���������� �������� ������ �������", e);
        }	
    }
}
