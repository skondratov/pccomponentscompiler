package ccp.complect.data.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import ccp.complect.data.entity.Answer;
import ccp.complect.data.entity.Question;

public class QuestionDAO extends DAO{
    public Question createQuestion(String descript)
            throws Exception {
        try {
            begin();
            Question que = new Question(descript);
            getSession().save(que);
            commit();
            return que;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("���������� �������� ������: " + descript, e);
        }
    }
    
    public Question updateQuestion(Question que)throws Exception {
        try {
            begin();
            getSession().update(que);
            commit();
            return que;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("���������� �������� ������: " + que.getDescript() + e.getMessage(), e);
        }
    }

    public Question getQuestion(String descript) throws Exception {
        try {
            begin();
            Query q = getSession().createQuery("from Question where descript = :descript");
            q.setString("descript", descript);
            Question que = (Question) q.uniqueResult();
            commit();
            return que;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("���������� �������� ������ �� ����:  " + descript, e);
        }
    }
    
    public Question getStartQuestion() throws Exception {
        try {
            begin();
            Query q = getSession().createQuery("from Question where qstart = 1");
            Question que = (Question) q.uniqueResult();
            commit();
            return que;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("���������� �������� ������ ������ �� ����", e);
        }
    }
    
    public List<Question> getAllQuestion() throws Exception{
        try {
            begin();
            Query q = getSession().createQuery("from Question");
            List<Question> ques = q.list();
            commit();
            return ques;
        } catch (HibernateException e) {
            rollback();
            throw new Exception("���������� �������� ������ ��������", e);
        }	
    }
}
