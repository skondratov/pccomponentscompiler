package ccp.complect.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="motherboard")
public class MotherBoard {
	private int id;
	private String pn;
	private int nbSlotRAM;
	private int nbSlotUSB2;
	private int nbSlotUSB3;
	private int nbSlotPCI;
	private int nbSlotPCIX1;
	private int nbSlotPCIX16;
	private int nbSlotMPCIe;
	private int nbSlotSATA;
	private int nbSlotDisplayPort;
	private int nbSlotDSub;
	private int nbSlotDVI;
	private int nbSlotHDMI;
	private int nbSlotESata;
	private int nbSlotCOM;
	private float cost;
	
	public MotherBoard() {
		super();
	}
	
	@Id
    @Column(name="ID")
    @GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name="P_N")
	public String getPn() {
		return pn;
	}

	public void setPn(String pn) {
		this.pn = pn;
	}

	@Column(name="NB_Slot_Ram")
	public int getNbSlotRAM() {
		return nbSlotRAM;
	}

	public void setNbSlotRAM(int nbSlotRAM) {
		this.nbSlotRAM = nbSlotRAM;
	}

	@Column(name="NB_Slot_USB2")
	public int getNbSlotUSB2() {
		return nbSlotUSB2;
	}

	public void setNbSlotUSB2(int nbSlotUSB2) {
		this.nbSlotUSB2 = nbSlotUSB2;
	}
	
	@Column(name="NB_Slot_USB3")
	public int getNbSlotUSB3() {
		return nbSlotUSB3;
	}

	public void setNbSlotUSB3(int nbSlotUSB3) {
		this.nbSlotUSB3 = nbSlotUSB3;
	}
	
	@Column(name="NB_Slot_PCI")
	public int getNbSlotPCI() {
		return nbSlotPCI;
	}

	public void setNbSlotPCI(int nbSlotPCI) {
		this.nbSlotPCI = nbSlotPCI;
	}

	@Column(name="NB_Slot_PCIX1")
	public int getNbSlotPCIX1() {
		return nbSlotPCIX1;
	}

	public void setNbSlotPCIX1(int nbSlotPCIX1) {
		this.nbSlotPCIX1 = nbSlotPCIX1;
	}

	@Column(name="NB_Slot_PCIX16")
	public int getNbSlotPCIX16() {
		return nbSlotPCIX16;
	}

	public void setNbSlotPCIX16(int nbSlotPCIX16) {
		this.nbSlotPCIX16 = nbSlotPCIX16;
	}

	@Column(name="NB_Slot_mPCIe")
	public int getNbSlotMPCIe() {
		return nbSlotMPCIe;
	}

	public void setNbSlotMPCIe(int nbSlotMPCIe) {
		this.nbSlotMPCIe = nbSlotMPCIe;
	}

	@Column(name="NB_Slot_SATA")
	public int getNbSlotSATA() {
		return nbSlotSATA;
	}

	public void setNbSlotSATA(int nbSlotSATA) {
		this.nbSlotSATA = nbSlotSATA;
	}

	@Column(name="NB_Slot_DisplayPort")
	public int getNbSlotDisplayPort() {
		return nbSlotDisplayPort;
	}

	public void setNbSlotDisplayPort(int nbSlotDisplayPort) {
		this.nbSlotDisplayPort = nbSlotDisplayPort;
	}

	@Column(name="NB_Slot_DSub")
	public int getNbSlotDSub() {
		return nbSlotDSub;
	}

	public void setNbSlotDSub(int nbSlotDSub) {
		this.nbSlotDSub = nbSlotDSub;
	}

	@Column(name="NB_Slot_DVI")
	public int getNbSlotDVI() {
		return nbSlotDVI;
	}

	public void setNbSlotDVI(int nbSlotDVI) {
		this.nbSlotDVI = nbSlotDVI;
	}

	@Column(name="NB_Slot_HDMI")
	public int getNbSlotHDMI() {
		return nbSlotHDMI;
	}

	public void setNbSlotHDMI(int nbSlotHDMI) {
		this.nbSlotHDMI = nbSlotHDMI;
	}

	@Column(name="NB_Slot_eSata")
	public int getNbSlotESata() {
		return nbSlotESata;
	}

	public void setNbSlotESata(int nbSlotESata) {
		this.nbSlotESata = nbSlotESata;
	}

	@Column(name="NB_Slot_COM")
	public int getNbSlotCOM() {
		return nbSlotCOM;
	}

	public void setNbSlotCOM(int nbSlotCOM) {
		this.nbSlotCOM = nbSlotCOM;
	}

	@Column(name="Cost_MB")
	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}
	
}
