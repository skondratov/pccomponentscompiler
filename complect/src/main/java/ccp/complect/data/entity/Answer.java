package ccp.complect.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Answer")
public class Answer {
	int id;
	String descript;
	Question nextQuestion;
	Question parentQuestion;
	 
	public Answer() {

	}

	public Answer(String descript) {
		this.descript = descript;
	}
	
	@Id
    @Column(name="ID")
    @GeneratedValue
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	@Column(name="descript")
	public String getDescript() {
		return descript;
	}
	public void setDescript(String descript) {
		this.descript = descript;
	}

	@ManyToOne
	@JoinColumn(name = "parentQuestion", nullable = false)
	public Question getParentQuestion() {
		return parentQuestion;
	}

	public void setParentQuestion(Question parentQuestion) {
		this.parentQuestion = parentQuestion;
	}

	@ManyToOne
	@JoinColumn(name = "nextQuestion", nullable = false)
	public Question getNextQuestion() {
		return nextQuestion;
	}

	public void setNextQuestion(Question nextQuestion) {
		this.nextQuestion = nextQuestion;
	}
	
	
}
