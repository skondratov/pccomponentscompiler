package ccp.complect.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="videocard")
public class VideoCard {
	private int id;
	private String pn;
	private float clockGPU;
	private float clockMemory;
	private float size;
	private float cost;
	private float widthBit;
	private Boolean chip;
	private String gddrType;
	private int nbDSub;
	private int nbDisplayPort;
	private int nbHDMI;
	private int nbDVI;
	
	public VideoCard() {
		super();
	}
	
	@Id
    @Column(name="ID")
    @GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name="P_N")
	public String getPn() {
		return pn;
	}

	public void setPn(String pn) {
		this.pn = pn;
	}

	@Column(name="Clock_GPU")
	public float getClockGPU() {
		return clockGPU;
	}

	public void setClockGPU(float clockGPU) {
		this.clockGPU = clockGPU;
	}

	@Column(name="Clock_Memory")
	public float getClockMemory() {
		return clockMemory;
	}

	public void setClockMemory(float clockMemory) {
		this.clockMemory = clockMemory;
	}

	@Column(name="Size_VC")
	public float getSize() {
		return size;
	}

	public void setSize(float size) {
		this.size = size;
	}

	@Column(name="Cost_VC")
	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	@Column(name="Width_Bit")
	public float getWidthBit() {
		return widthBit;
	}

	public void setWidthBit(float widthBit) {
		this.widthBit = widthBit;
	}
	
	@Column(name="Chip")
	public Boolean getChip() {
		return chip;
	}

	public void setChip(Boolean chip) {
		this.chip = chip;
	}

	@Column(name="Type_GDDR")
	public String getGddrType() {
		return gddrType;
	}

	public void setGddrType(String gddrType) {
		this.gddrType = gddrType;
	}

	@Column(name="NB_Dsub")
	public int getNbDSub() {
		return nbDSub;
	}

	public void setNbDSub(int nbDSub) {
		this.nbDSub = nbDSub;
	}
	
	@Column(name="NB_DisplayPort")
	public int getNbDisplayPort() {
		return nbDisplayPort;
	}

	public void setNbDisplayPort(int nbDisplayPort) {
		this.nbDisplayPort = nbDisplayPort;
	}

	@Column(name="NB_HDMI")
	public int getNbHDMI() {
		return nbHDMI;
	}

	public void setNbHDMI(int nbHDMI) {
		this.nbHDMI = nbHDMI;
	}

	@Column(name="NB_DVI")
	public int getNbDVI() {
		return nbDVI;
	}

	public void setNbDVI(int nbDVI) {
		this.nbDVI = nbDVI;
	}
}
