package ccp.complect.data.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Question")
public class Question {
	private int id;
	private String descript;
	private Set<Answer> answers = new HashSet<Answer>(0);
	private Boolean qstart;
	
	public Question() {
		
	}
	
	public Question(String descript) {
		this.descript = descript;
	}

	@Id
    @Column(name="ID")
    @GeneratedValue
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="descript")
	public String getDescript() {
		return descript;
	}
	public void setDescript(String descript) {
		this.descript = descript;
	}

	@OneToMany(mappedBy = "parentQuestion")
	public Set<Answer> getAnswers() {
		return answers;
	}
	public void setAnswers(Set<Answer> answers) {
		this.answers = answers;
	}

	@Column
	public Boolean getQstart() {
		return qstart;
	}

	public void setQstart(Boolean qstart) {
		this.qstart = qstart;
	}
}
