package ccp.complect.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="computer")
public class Computer {
	private int id;
	private float cost;
	private CPU cpu;
	private HDD hdd;
	private MotherBoard motherBoard;
	private PSU psu;
	private RAM ram;
	private VideoCard videoCard;
	private float totalRank;
	
	public Computer() {
		super();
	}
	
	@Id
    @Column(name="ID")
    @GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name="Cost")
	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	@ManyToOne
	@JoinColumn(name = "cpu")
	public CPU getCpu() {
		return cpu;
	}

	public void setCpu(CPU cpu) {
		this.cpu = cpu;
	}

	@ManyToOne
	@JoinColumn(name = "hdd")
	public HDD getHdd() {
		return hdd;
	}

	public void setHdd(HDD hdd) {
		this.hdd = hdd;
	}

	@ManyToOne
	@JoinColumn(name = "motherBoard")
	public MotherBoard getMotherBoard() {
		return motherBoard;
	}

	public void setMotherBoard(MotherBoard motherBoard) {
		this.motherBoard = motherBoard;
	}

	@ManyToOne
	@JoinColumn(name = "psu")
	public PSU getPsu() {
		return psu;
	}

	public void setPsu(PSU psu) {
		this.psu = psu;
	}

	@ManyToOne
	@JoinColumn(name = "ram")
	public RAM getRam() {
		return ram;
	}

	public void setRam(RAM ram) {
		this.ram = ram;
	}
	
	@ManyToOne
	@JoinColumn(name = "videoCard")
	public VideoCard getVideoCard() {
		return videoCard;
	}

	public void setVideoCard(VideoCard videoCard) {
		this.videoCard = videoCard;
	}

	@Column(name="Total_Rank")
	public float getTotalRank() {
		return totalRank;
	}

	public void setTotalRank(float totalRank) {
		this.totalRank = totalRank;
	}
	
}
