package ccp.complect.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="hdd")
public class HDD {
	private int id;
	private String pn;
	private float size;
	private float cache;
	private float speed;
	private float cost;
	private String sataType;
	private String hddType;
	
	public HDD() {
		super();
	}
	
	@Id
    @Column(name="ID")
    @GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="P_N")
	public String getPn() {
		return pn;
	}

	public void setPn(String pn) {
		this.pn = pn;
	}
	
	@Column(name="Size")
	public float getSize() {
		return size;
	}

	public void setSize(float size) {
		this.size = size;
	}

	@Column(name="Cache_HDD")
	public float getCache() {
		return cache;
	}

	public void setCache(float cache) {
		this.cache = cache;
	}

	@Column(name="Speed_HDD")
	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	@Column(name="Cost_HDD")
	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	@Column(name="Type_Sata")
	public String getSataType() {
		return sataType;
	}

	public void setSataType(String sataType) {
		this.sataType = sataType;
	}

	@Column(name="Type_HDD")
	public String getHddType() {
		return hddType;
	}

	public void setHddType(String hddType) {
		this.hddType = hddType;
	}
}
