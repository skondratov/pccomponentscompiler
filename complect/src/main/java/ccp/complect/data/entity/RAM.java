package ccp.complect.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ram")
public class RAM {
	private int id;
	private String pn;
	private float clock;
	private float size;
	private float cost;
	private float timing;
	private float nbKit;
	
	public RAM() {
		super();
	}
	
	@Id
    @Column(name="ID")
    @GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name="P_N")
	public String getPn() {
		return pn;
	}

	public void setPn(String pn) {
		this.pn = pn;
	}

	@Column(name="Clock_RAM")
	public float getClock() {
		return clock;
	}

	public void setClock(float clock) {
		this.clock = clock;
	}

	@Column(name="Size_RAM")
	public float getSize() {
		return size;
	}

	public void setSize(float size) {
		this.size = size;
	}

	@Column(name="Cost_RAM")
	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}
	
	@Column(name="Timing_RAM")
	public float getTiming() {
		return timing;
	}

	public void setTiming(float timing) {
		this.timing = timing;
	}

	@Column(name="NB_Kit_RAM")
	public float getNbKit() {
		return nbKit;
	}

	public void setNbKit(float nbKit) {
		this.nbKit = nbKit;
	}
	
	
}
