package ccp.complect.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cpu")
public class CPU {
	private int id;
	private String pn;
	private int cache;
	private float cost;
	private float clock;
	private float tdp;
	private int nbCore;
	
	public CPU() {
		super();
	}
	
	@Id
    @Column(name="ID")
    @GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name="P_N")
	public String getPn() {
		return pn;
	}

	public void setPn(String pn) {
		this.pn = pn;
	}

	@Column(name="Cache")
	public int getCache() {
		return cache;
	}

	public void setCache(int cache) {
		this.cache = cache;
	}
	
	@Column(name="Cost_CPU")
	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}
	
	@Column(name="CPU_Clock")
	public float getClock() {
		return clock;
	}

	public void setClock(float clock) {
		this.clock = clock;
	}
	
	@Column(name="TDP")
	public float getTdp() {
		return tdp;
	}

	public void setTdp(float tdp) {
		this.tdp = tdp;
	}
	
	@Column(name="NB_Core_CPU")
	public int getNbCore() {
		return nbCore;
	}

	public void setNbCore(int nbCore) {
		this.nbCore = nbCore;
	}	
}
