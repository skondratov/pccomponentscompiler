package ccp.complect.data.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="psu")
public class PSU {
	private int id;
	private String pn;
	private float power;
	private float coolerSize;
	private float cost;
	private int nbSata;
	private int nbPCIe;
	
	public PSU() {
		super();
	}

	@Id
    @Column(name="ID")
    @GeneratedValue
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	@Column(name="P_N")
	public String getPn() {
		return pn;
	}

	public void setPn(String pn) {
		this.pn = pn;
	}

	@Column(name="Power")
	public float getPower() {
		return power;
	}

	public void setPower(float power) {
		this.power = power;
	}

	@Column(name="CoolerSize")
	public float getCoolerSize() {
		return coolerSize;
	}

	public void setCoolerSize(float coolerSize) {
		this.coolerSize = coolerSize;
	}

	@Column(name="Cost_PSU")
	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	@Column(name="NB_Sata")
	public int getNbSata() {
		return nbSata;
	}

	public void setNbSata(int nbSata) {
		this.nbSata = nbSata;
	}
	
	@Column(name="NB_PCIe")
	public int getNbPCIe() {
		return nbPCIe;
	}

	public void setNbPCIe(int nbPCIe) {
		this.nbPCIe = nbPCIe;
	}
	
	
}
